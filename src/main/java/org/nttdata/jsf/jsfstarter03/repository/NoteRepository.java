package org.nttdata.jsf.jsfstarter03.repository;

import org.nttdata.jsf.jsfstarter03.models.Note;
import org.springframework.data.jpa.repository.JpaRepository;

public interface NoteRepository extends JpaRepository<Note, Integer> {
	
}
