package org.nttdata.jsf.jsfstarter03.repository;

import org.nttdata.jsf.jsfstarter03.models.Person;
import org.springframework.data.jpa.repository.JpaRepository;

public interface PersonRepository extends JpaRepository<Person, Integer> {

}
