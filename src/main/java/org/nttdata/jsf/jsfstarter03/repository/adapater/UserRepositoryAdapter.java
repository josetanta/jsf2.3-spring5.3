package org.nttdata.jsf.jsfstarter03.repository.adapater;

import java.util.List;
import java.util.Optional;

import javax.persistence.Query;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.nttdata.jsf.jsfstarter03.models.User;
import org.nttdata.jsf.jsfstarter03.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.client.ResourceAccessException;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Repository("userRepository")
@Transactional
public class UserRepositoryAdapter implements UserRepository {

	@Autowired
	private SessionFactory sessionFactory;
	private Session session;

	@Override
	public void createUser(User user) {
		log.info("persist a new user - {}", user);
		session.beginTransaction();
		session.save(user);
		session.getTransaction().commit();
	}

	@Override
	public Optional<User> findByUsernameAndPassword(String username, String password) {
		session = sessionFactory.openSession();
		String jpql = "FROM User u WHERE u.username = :username AND u.password = :password";

		session.beginTransaction();
		Query findUser = session.createQuery(jpql);
		findUser.setParameter("username", username);
		findUser.setParameter("password", password);

		List<User> users = (List<User>) findUser.getResultList();
		session.getTransaction().commit();

		if (users.isEmpty())
			throw new ResourceAccessException("This user does not exist or is not registered");

		return users.stream().findFirst();
	}

}
