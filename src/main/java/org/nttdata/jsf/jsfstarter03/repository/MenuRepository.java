package org.nttdata.jsf.jsfstarter03.repository;

import org.nttdata.jsf.jsfstarter03.models.Menu;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository("menuRepository")
public interface MenuRepository extends JpaRepository<Menu, Integer> {

}
