package org.nttdata.jsf.jsfstarter03.repository;

import org.nttdata.jsf.jsfstarter03.models.Category;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CategoryRepository extends JpaRepository<Category, Integer> {
	
}
