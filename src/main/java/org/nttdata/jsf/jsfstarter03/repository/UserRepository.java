package org.nttdata.jsf.jsfstarter03.repository;

import java.util.Optional;

import org.nttdata.jsf.jsfstarter03.models.User;

public interface UserRepository {
	void createUser(User user);

	Optional<User> findByUsernameAndPassword(String username, String password);
}
