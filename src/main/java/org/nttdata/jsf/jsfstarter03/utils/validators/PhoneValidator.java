package org.nttdata.jsf.jsfstarter03.utils.validators;

import java.util.regex.Pattern;

import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.validator.FacesValidator;
import javax.faces.validator.Validator;
import javax.faces.validator.ValidatorException;

@FacesValidator("phoneValidator")
public class PhoneValidator implements Validator {

	@Override
	public void validate(FacesContext context, UIComponent component, Object value) throws ValidatorException {
		String phone = value.toString().trim();
		if (phone.length() == 0)
			throw new ValidatorException(new FacesMessage("Enter your cell phone"));
		else {
			String pattern = "^9[0-9]{8}$";
			boolean ok = Pattern.matches(pattern, phone);
			if (!ok)
				throw new ValidatorException(new FacesMessage("Invalid format"));
		}
	}
}
