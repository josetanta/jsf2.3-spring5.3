package org.nttdata.jsf.jsfstarter03.models;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "notes")
public class Note {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;

	private String header;
	private String body;
	private Date createdAt;
	private String comment;
	private String commentOfAdmin;
	private short valorisation;

	@ManyToOne
	@JoinColumn(nullable = false)
	private Person owner;

	@ManyToOne
	@JoinColumn(nullable = false)
	private Category category;
}
