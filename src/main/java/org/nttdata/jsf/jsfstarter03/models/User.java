package org.nttdata.jsf.jsfstarter03.models;

import java.io.Serializable;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Entity
@Table(name = "users")
@AllArgsConstructor
@NoArgsConstructor
public class User implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@OneToOne(cascade = CascadeType.PERSIST)
	@JoinColumn(name = "id", nullable = false)
	private Person owner;

	private String username;
	private String password;
	private String type;
	private short status = 1;
}
