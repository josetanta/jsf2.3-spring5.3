package org.nttdata.jsf.jsfstarter03.services;

import java.util.List;

import org.nttdata.jsf.jsfstarter03.exceptions.ResourceNotFoundException;
import org.nttdata.jsf.jsfstarter03.models.Person;
import org.nttdata.jsf.jsfstarter03.models.User;
import org.nttdata.jsf.jsfstarter03.repository.PersonRepository;
import org.nttdata.jsf.jsfstarter03.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class EmployeeServiceAdapter implements EmployeeService {

	@Autowired
	private PersonRepository personRepository;

	@Autowired
	private UserRepository userRepository;

	@Override
	public List<Person> listEmployees() {
		return personRepository.findAll();
	}

	@Override
	public void registerPerson(Person person) {
		log.info("register new person {}", person);
		personRepository.save(person);
	}

	@Override
	public void registerUser(User user) {
		personRepository.save(user.getOwner());
		userRepository.createUser(user);
	}

	@Override
	public User login(User user) throws ResourceNotFoundException {
		return userRepository
				.findByUsernameAndPassword(user.getUsername(), user.getPassword())
				.orElseThrow(() -> new ResourceNotFoundException("Doest exist this user."));
	}
}
