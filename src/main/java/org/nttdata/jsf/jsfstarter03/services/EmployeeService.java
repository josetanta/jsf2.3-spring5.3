package org.nttdata.jsf.jsfstarter03.services;

import java.util.List;

import org.nttdata.jsf.jsfstarter03.exceptions.ResourceNotFoundException;
import org.nttdata.jsf.jsfstarter03.models.Person;
import org.nttdata.jsf.jsfstarter03.models.User;

public interface EmployeeService {
	List<Person> listEmployees();

	void registerPerson(Person person);

	void registerUser(User user);

	User login(User user) throws ResourceNotFoundException;
}
