package org.nttdata.jsf.jsfstarter03.controllers;

import java.io.Serializable;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;

import org.nttdata.jsf.jsfstarter03.models.User;
import org.nttdata.jsf.jsfstarter03.services.EmployeeService;

import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;

@Getter
@Setter
@Slf4j
@ManagedBean
@ViewScoped
public class IndexController implements Serializable {

	@ManagedProperty(value = "#{administerEmployee}")
	transient private EmployeeService employeeService;

	private User user;

	@PostConstruct
	public void init() {
		user = new User();
	}

	public String login() {
		var facesInstance = FacesContext.getCurrentInstance();

		User findUser = null;
		try {
			findUser = employeeService.login(user);
			facesInstance.getExternalContext().getSessionMap().put("session-us", findUser);
			log.info("infor user {}", findUser);
			return "/home?faces-redirect=true";
		} catch (RuntimeException ex) {
			facesInstance.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_WARN, "Aviso", ex.getMessage()));
			log.error("error {}", ex.getMessage());
		}
		return null;
	}
}
