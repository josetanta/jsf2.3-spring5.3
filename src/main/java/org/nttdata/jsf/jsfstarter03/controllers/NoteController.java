package org.nttdata.jsf.jsfstarter03.controllers;

import java.io.Serializable;
import java.util.List;

import javax.faces.view.ViewScoped;

import org.nttdata.jsf.jsfstarter03.models.Category;
import org.nttdata.jsf.jsfstarter03.models.Note;
import org.nttdata.jsf.jsfstarter03.models.Person;
import org.nttdata.jsf.jsfstarter03.repository.CategoryRepository;
import org.nttdata.jsf.jsfstarter03.repository.NoteRepository;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
//@ManagedBean
@ViewScoped
public class NoteController implements Serializable {

	private static final long serialVersionUID = 1L;

//	@ManagedProperty("#{categoryRepository}")
//	transient private CategoryRepository categoryRepository;

//	@ManagedProperty("#{noteRepository}")
//	transient private NoteRepository noteRepository;

	private CategoryRepository categoryRepository;

	private NoteRepository noteRepository;

	private Note note;
	private Category category;
	private Person person;

	private List<Category> categories;
	private List<Note> notes;

//	@PostConstruct
//	private void init() {
//		setNote(new Note());
//		setCategory(new Category());
//		setPerson(new Person());
//
//		setCategories(categoryRepository.findAll());
//		setNotes(noteRepository.findAll());
//	}
}
