package org.nttdata.jsf.jsfstarter03.controllers;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;

import org.nttdata.jsf.jsfstarter03.exceptions.ResourceNotFoundException;
import org.nttdata.jsf.jsfstarter03.models.Note;
import org.nttdata.jsf.jsfstarter03.repository.NoteRepository;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;

@Getter
@Setter
@ManagedBean(name = "valueMB", eager = true)
@Slf4j
@ViewScoped
@EqualsAndHashCode
public class ValueController {

	@ManagedProperty("#{mentionMB}")
	transient private MentionController mentionController;

	@ManagedProperty("#{noteRepository}")
	transient private NoteRepository noteRepository;

	// FIXME: Not binding field note with actionListener
	@ManagedProperty("#{note}")
	private Note note;

	@PostConstruct
	public void init() {
		setNote(mentionController.getNote());
		log.info("init value-controller ({})", getNote());
	}

	public void registerNote() {
		var facesCurrent = FacesContext.getCurrentInstance();
		log.info("update note with comment of admin note - {}", getNote());
		try {
			noteRepository.save(note);
			facesCurrent.addMessage(null,
					new FacesMessage(FacesMessage.SEVERITY_INFO, "Success", "Register note with successfully."));
		} catch (ResourceNotFoundException ex) {
			facesCurrent.addMessage(null,
					new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error", "There are anything error."));
		}
	}
}
