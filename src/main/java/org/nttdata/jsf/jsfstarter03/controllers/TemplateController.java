package org.nttdata.jsf.jsfstarter03.controllers;

import java.io.Serializable;
import java.util.Objects;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;

import org.nttdata.jsf.jsfstarter03.models.User;

@ManagedBean
@SessionScoped
public class TemplateController implements Serializable {

	public void isAuth() {
		var facesCurrent = FacesContext.getCurrentInstance().getExternalContext();
		try {
			User user = (User) facesCurrent.getSessionMap().get("session-us");
			if (Objects.isNull(user))
				facesCurrent.redirect(facesCurrent.getRequestContextPath() + "/permissions.myapp");
		} catch (Exception e) {
			// errrors
		}
	}
}
