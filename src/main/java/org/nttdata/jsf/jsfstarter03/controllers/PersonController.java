package org.nttdata.jsf.jsfstarter03.controllers;

import java.io.Serializable;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.RequestScoped;

import org.nttdata.jsf.jsfstarter03.models.Person;
import org.nttdata.jsf.jsfstarter03.models.User;
import org.nttdata.jsf.jsfstarter03.services.EmployeeService;

import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;

@Getter
@Setter
@Slf4j
@ManagedBean(name = "personMB")
@RequestScoped
public class PersonController implements Serializable {

	private static final long serialVersionUID = 1L;

	@ManagedProperty("#{administerEmployee}")
	EmployeeService employeeService;

	private User user;
	private Person person;
	private List<Person> people;

	@PostConstruct
	public void init() {
		person = new Person();
		user = new User();
		people = employeeService.listEmployees();
		log.info("init values for person, user and people [{}]", getPeople());
	}

	public String register() {
		user.setOwner(getPerson());
		employeeService.registerUser(getUser());
		return "result-information?faces-redirect=true";
	}
}
