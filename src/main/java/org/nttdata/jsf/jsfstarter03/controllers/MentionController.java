package org.nttdata.jsf.jsfstarter03.controllers;

import java.io.Serializable;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.RequestScoped;

import org.nttdata.jsf.jsfstarter03.models.Note;
import org.nttdata.jsf.jsfstarter03.repository.NoteRepository;

import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;

@Getter
@Setter
@Slf4j
@ManagedBean(name = "mentionMB")
@RequestScoped
public class MentionController implements Serializable {
	private static final long serialVersionUID = 1L;

	@ManagedProperty("#{noteRepository}")
	transient private NoteRepository noteRepository;

	private List<Note> notes;
	private Note note;

	@PostConstruct
	public void init() {
		var notesAll = noteRepository.findAll();
		setNotes(notesAll);
		log.info("init all notes {}", notesAll);
	}

	public void assign(Note note) {
		log.info("info for setting node {}", note);
		setNote(note);
	}
}
