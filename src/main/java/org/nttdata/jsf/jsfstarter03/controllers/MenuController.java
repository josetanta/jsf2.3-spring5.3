package org.nttdata.jsf.jsfstarter03.controllers;

import java.io.Serializable;
import java.util.List;
import java.util.Objects;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;

import org.nttdata.jsf.jsfstarter03.models.Menu;
import org.nttdata.jsf.jsfstarter03.models.User;
import org.nttdata.jsf.jsfstarter03.repository.MenuRepository;
import org.primefaces.model.menu.DefaultMenuItem;
import org.primefaces.model.menu.DefaultMenuModel;
import org.primefaces.model.menu.DefaultSubMenu;
import org.primefaces.model.menu.MenuModel;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@ManagedBean
@SessionScoped
public class MenuController implements Serializable {

	private static final long serialVersionUID = 1L;

	@ManagedProperty("#{menuRepository}")
	transient private MenuRepository menuRepository;

	private List<Menu> menuList;
	private MenuModel menuModel;

	@PostConstruct
	public void init() {
		setMenuList(menuRepository.findAll());
		setMenuModel(new DefaultMenuModel());

		buildMenuList(getMenuList());
	}

	public void logout() {
		FacesContext.getCurrentInstance().getExternalContext().invalidateSession();
	}

	private void buildMenuList(List<Menu> menuList) {
		// type menu is: 'S', 'I' ( S: Submenu, I: Item )
		// type submenu is: 'A', 'U', 'M' these are users' types
		var ce = FacesContext.getCurrentInstance().getExternalContext();
		User user = (User) ce.getSessionMap().get("session-us");

		if (Objects.isNull(user))
			return;

		var appPath = ce.getApplicationContextPath();

		menuList.forEach(m01 -> {
			boolean isSubmenu = m01.getType().equals("S");
			boolean typeuser = m01.getTypeUser().equals(user.getType());

			if (isSubmenu && typeuser) {
				DefaultSubMenu newsubmenu = DefaultSubMenu.builder().label(m01.getName()).id(m01.getId().toString())
						.build();

				menuList.forEach(m02 -> {
					boolean isItem = m02.getType().equals("I");
					boolean hasSubMenu = !Objects.isNull(m02.getSubMenu());
					boolean typeuser02 = m02.getTypeUser().equals(user.getType());

					if (isItem && hasSubMenu && m02.getSubMenu().getId().equals(m01.getId()) && typeuser02) {
						DefaultMenuItem newItem = DefaultMenuItem.builder().value(m02.getName())
								.url(appPath + m02.getUrl()).build();
						newsubmenu.getElements().add(newItem);
					}
				});

				menuModel.getElements().add(newsubmenu);
			} else if (!isSubmenu && Objects.isNull(m01.getSubMenu()) && typeuser) {
				DefaultMenuItem newItem = DefaultMenuItem.builder().value(m01.getName()).url(appPath + m01.getUrl())
						.build();
				menuModel.getElements().add(newItem);
			}
		});
	}
}
